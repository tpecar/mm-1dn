function p = parametri(x,y,model,k)
  % Izracuna najprimernejse parametere podanemu matematicnemu modelu po metodi
  % najmanjsih kvadratov glede na znane vzorcne tocke pojava, ki ga model skusa
  % zajeti.
  % ----------------------------------------------------------------------------
  % VHOD:
  %      x - vektor neodvisnih spremenljivk izmerjenih vzorcnih tock
  %      y - vektor odvisnih spremenljivk izmerjenih vzorcnih tock
  % @model - funkcija @(p, x), ki za podan vektor parametrov p ter vrednost
  %          neodvisne spremenljivke x vrne po modelu izracunano vrednost
  %          odvisne spremenljivke
  %      k - stevilo parametrov podanega modela
  % IZHOD:
  %      vektor parametrov modela velikosti [k:1]

  % tvorimo matriko s poracunanimi cleni modela glede na vrednosti neodvisnih
  % spremenljivk vzorcnih tock
  % gremo po clenih, s cimer racunamo po stolpcih
  for i=1:k
    A(:,i) = model([zeros(i-1,1); 1; zeros(k-i,1)], x);
  end
  % nareimo Moore-Penroseov inverz A ter preko mnozenja z y z leve strani
  % dobimo parametre, ki so najblizji resitvi sistema Ax=y (tj. da za dan model
  % vrne najmanjse odstopanje od vrednosti odvisnih spremenljivk - desne strani,
  % kar je ekvivalentno resevanju po metodi najmanjsih kvadratov)
  p = pinv(A) * y;
endfunction
% testi
% premica
%!assert(parametri([1;2;3],[1;2;3],@(p,x)(1)*x,1), [1], eps)

% kvadratna funkcija
%!assert(parametri([0;1;2],[0;-1;0],@(p,x) p(1)+p(2)*x+p(3)*x.^2, 3), [0;-2;1], 1e-9)

% kotna funkcija
%!assert(parametri([0;pi;4*pi],[0;2;0],@(p,x) p(1)*sin(x./2), 1), [2], 1e-9)

% polinom
%!test
%! p = [1;2;3;4];
%! f = @(p,x) p(1) + p(2)*x + p(3)*x.^2 + p(4)*x.^5;
%! vhod = [5;4;3;2;1];
%! izhod = f(p,vhod);
%! assert(parametri(vhod, izhod, f, 4), p, 1e-9)

% polinom z nakljucnimi parametri
%!test
%! p = rand(10,1);
%! f = @(p,x) p(1) + p(2)*x.^2 + p(3)*x.^3 + p(4)*x.^4 + p(5)*x.^5 ...
%!          + p(6)*x.^6 + p(7)*x.^7 + p(8)*x.^8 + p(9)*x.^9 + p(10)*x.^10;
%! vhod = rand(200,1);
%! izhod = f(p,vhod);
%! assert(parametri(vhod, izhod, f, 10), p, 1e-9)
